import { describe, it, test, expect } from 'vitest';
import { subnetmask } from './subnetmask.js';

describe('hostbits test', () => {
    test('reverse von [1,2,3] ist [3,2,1]', () => {
        expect(subnetmask.hostbits(24)).toBe(0);
    });

});